<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="ico/favicon.png">

    <title>NAS Test</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.css">

    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/bootstrap.js" ></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php
    include('php/controller/controller.php');
?>

<div class="main-wrapper">
    <div class="bg-100"></div>

    <div class="navigation">

        <div class="navbar">

            <a class="logo brand" href="index.php">
                <img src="images/logo.png" alt="" /><!-- IF YOU NEED TEXT LOGO UNCOMMENTED THIS {APTURE} -->
            </a>

                <button type="button" class="nav navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


                <div class="nav navbar-collapse collapse">
                    <ul class="nav nav-pills nav-stacked menu-item" id="top-menu">
                    </ul>
                </div>
        </div>
    </div>

    <!-- ######################## CONTENT SIDE ##################
       ============================================================== -->
    <div id="content">
        <div class="wrapper">
            <div class="container">
                <?php
                // $_GET und $_POST zusammenfasen
                $request = array_merge($_GET, $_POST);
                // Controller erstellen
                $controller = new Controller($request);
                // Inhalt der Webanwendung ausgeben.
                echo $controller->display();
                ?>
            </div>
        </div><!-- WRAPPER -->
    </div><!-- CONTENT-->
<!-- END: MAIN-WRAPPER-->
</div>

<?php foreach(Routes::getRoutes() as $view => $entry){ ?>
    <script>
        $_main =
            $('<li><a class="tree-toggler btn btn-primary btn-sm" ><?php echo $entry['title']; ?></a ><ul class="tree collapse"></ul></li>');
    </script>
<?php foreach(Routes::getRoute($view)['model'] as $id => $entry1) { ?>
<?php { ?>
<?php if(array_key_exists('href',$entry1[0])){ ?>
    <script>
        $_sub =
            $('<li><a class="btn btn-primary btn-sm btn-block" href="<?php echo $entry1[0]['href'] ?>" title=""><?php echo I8N::translate(Routes::$curLanguage,$id) ?></a></li>');
        $_main.find('ul.tree').append($_sub);
    </script>
<?php } else {?>
    <script>

        $_sub =
            $('<li><a class="btn btn-primary btn-sm btn-block" title="" onclick="changeContent(\'<?php echo $view ?>\',\'<?php echo $id ?>\')"><?php echo I8N::translate(Routes::$curLanguage,$id) ?></a></li>');
        $_main.find('ul.tree').append($_sub);
    </script>
<?php } ?>
<?php } ?>
<?php } ?>
    <script>
        $('#top-menu').append($_main);
    </script>
<?php } ?>

<script>

    function changeContent(view,id){
        window.location.href = "index.php?view="+view+"&id="+id;
    }

    $(document).ready(function () {

        $('.tree-toggler').on('click',function () {
            $(this).parent().children('ul.tree').toggle(300);
        });
    });
</script>

</body>
</html>

