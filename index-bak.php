<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>NAS Test</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="responsive parallax Bootstrap template">
    <meta name="author" content="ArtLabs">

    <!-- ######################## Styles ######################
        =======================================================-->
    <link href="css/bootstrap-1.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">


    <!-- ####################################################################
        !important THIS FILE -= style.css =- ON BOTTOM OF BOOTSTRAP STYLEs  LIST
        need to override bootstrap style
    ===================================================-->
    <link href="css/style.css" rel="stylesheet">

    <!-- FontAwesome styles (iconic font)-->
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link href="css/index.css" rel="stylesheet">
    <!-- prettyPhoto styles -->

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="ico/favicon.png">

</head>

<body>


<!-- Le javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="js/jquery-2.0.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap-1.js" type="text/javascript"></script>
<script src="js/modernizr.custom.js" type="text/javascript"></script>

<!--################# START MAIN-WRAPPER ####################
	============================================================ -->

<div class="main-wrapper">
    <div class="bg-100"></div><!-- MENU BACKGROUND BOX-->
    <!-- NAV WRAPPER -->
    <div class="navigation">
        <!--LOGO-->
        <a class="logo brand" href="index.php">
            <img src="images/logo.png" alt="" /><!-- IF YOU NEED TEXT LOGO UNCOMMENTED THIS {APTURE} -->
        </a>

        <!-- NAVBAR -->
        <div class="menu-fix-on-scroll navbar">

            <!-- COLAPSE BUTTON -->
            <span class="mobile-btn"><!-- CENTERED SPAN-->
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </span>
            <!-- MENU-->

            <ul class="nav nav-collapse collapse" id="top-menu"></ul>
        </div>
    </div>



    <!-- ######################## CONTENT SIDE ##################
        ============================================================== -->
    <div id="content">
        <div class="wrapper">
            <div id="container-folio" class="scroll-content">
                <div class="container" id="container">
                    <?php
                        // unsere Klassen einbinden
                        include('php/controller/controller.php');

                        // $_GET und $_POST zusammenfasen
                        $request = array_merge($_GET, $_POST);
                        // Controller erstellen
                        $controller = new Controller($request);
                        // Inhalt der Webanwendung ausgeben.
                        echo $controller->display();
                    ?>
                </div><!-- CONTAINER-->
            </div><!-- CONTAINER FOLIO-->
        </div><!-- WRAPPER -->
    </div><!-- CONTENT-->
</div>
<!-- END: MAIN-WRAPPER-->

<?php foreach(Routes::getRoutes() as $view => $entry){ ?>
    <script>
        $_main =
        $('<li><label class="tree-toggle tree-header"><?php echo $entry['title']; ?></label> \
            <ul class="nav nav-collapse collapse tree"></ul>\
        </li><li class="divider"></li>');
    </script>
    <?php foreach(Routes::getRoute($view)['model'] as $id => $entry1) { ?>
            <?php { ?>
            <?php if(array_key_exists('href',$entry1[0])){ ?>
                <script>
                    $_sub =
                        $('<li><a href="<?php echo $entry1[0]['href'] ?>" title=""><?php echo I8N::translate(Routes::$curLanguage,$id) ?></a><span class="link-bg"></span></li>');
                    $_main.find('.nav.nav-collapse.collapse.tree').append($_sub);
                </script>
            <?php } else {?>
                <script>

                    $_sub =
                    $('<li><a title="" onclick="changeContent(\'<?php echo $view ?>\',\'<?php echo $id ?>\')"><?php echo I8N::translate(Routes::$curLanguage,$id) ?></a><span class="link-bg"></span></li>');
                    $_main.find('.nav.nav-collapse.collapse.tree').append($_sub);
                </script>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    <script>
        $('#top-menu').append($_main);
    </script>
<?php } ?>


<script>

    function changeContent(view,id){
       // $('#container').load(url);
        window.location.href = "index.php?view="+view+"&id="+id;
        //$.get('index.php',{view:view,id:id});
    }

    $(function() {

        $('.tree-toggle').each(function() {
            $(this).parent().children('ul.tree').toggle(200);
        });

        /*
         *  uncollapse menu
         */
        $('.tree-toggle').click(function () {
            $(this).parent().children('ul.tree').toggle(200);
        });
    });


</script>

</body>
</html>
