<div id="portfolio" class="html_container">
    <div class="row-fluid">

<?php
foreach($this->_['entries'] as $entry){
?>

    <ul class="media-grid">
        <li class="span3">
            <a href="#" class="thumbnail">
                <img src="<?php echo $entry['image'] ?>" alt="">
                <h3><?php echo I8N::translate(Routes::$curLanguage,$entry['title']) ?></h3>
            </a>
        </li>
    </ul>
<?php
}
?>
    </div>
</div>