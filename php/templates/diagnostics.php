<?php $view = new View();
//$view->load('system','test');
?>

<div class="html_container">
    <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul id="tabs" class="nav nav-tabs bg-200"></ul>
        <div id="tabcontent" class="tab-content"></div>
        <?php
        foreach($this->_['entries'] as $key=>$entry){
        ?>
            <script>
                $('<li><a href="#tab<?php echo $entry['id'] ?>" data-toggle="tab"><?php echo I8N::translate(Routes::$curLanguage,$entry['title']) ?></a></li>').appendTo('#tabs');
            </script>
                <?php if(array_key_exists('view',$entry)){  ?>
                <div class="tab-pane" id="tab<?php echo $entry['id'] ?>">
                   <?php echo $view->load('diagnostics',$entry['view']) ?>
                </div>
            <script>
                var element = $('#tab<?php echo $entry['id'] ?>').detach();
                $('#tabcontent').append(element);
            </script>
                <?php } else { ?>
            <script>
                $('<div class="tab-pane" id="tab<?php echo $entry['id'] ?>"><?php echo $entry['content'] ?></div>').appendTo('#tabcontent');
                <?php } ?>
            </script>
        <?php
        }
        ?>
        <script>
            $( "#tabs li:first-child").addClass('active');
            $( "#tabcontent div:first-child").addClass('active');
        </script>
    </div>
</div>
