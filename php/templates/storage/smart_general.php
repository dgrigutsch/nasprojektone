<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('smart_general_settings') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">

            <fieldset>
                <legend><?php echo I8N::_translate('smart_general_settings') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('smart_activate') ?></label>
                    <label>
                        <input type="checkbox">
                    </label>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('smart_intervall') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                        <span class="help-block"><?php echo I8N::_translate('smart_intervall_info') ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('smart_powermode') ?></label>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> <span class="caret"></span></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                </ul>
                            </div>
                        </div><!-- /btn-group -->
                        <span class="help-block"><?php echo I8N::_translate('smart_powermode_info') ?></span>
                    </div>
                </div>

            </fieldset>
            <fieldset>
                <legend><?php echo I8N::_translate('smart_temperaturemonitoring') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('smart_difference') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                        <span class="help-block"><?php echo I8N::_translate('smart_difference_info') ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('smart_info') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                        <span class="help-block"><?php echo I8N::_translate('smart_info_info') ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('smart_critical') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                        <span class="help-block"><?php echo I8N::_translate('smart_critical_info') ?></span>
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
