<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('logs_syslog') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">
            <fieldset>
                <legend><?php echo I8N::_translate('logs_syslog') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('logs_activate') ?></label>
                    <label>
                        <input type="checkbox">
                    </label>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('host') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('port') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('protocol') ?></label>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> <span class="caret"></span></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                </ul>
                            </div>
                        </div><!-- /btn-group -->
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>