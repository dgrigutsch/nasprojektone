<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('general_settings') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">

            <fieldset>
                <div class="form-group">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <label>
                                <span class=""><?php echo I8N::_translate('current_time') ?></span>
                                <span class="">Mi 30. Okt 17:16:53 CET 2013</span>
                            </label>
                        </div><!-- /btn-group -->
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend><?php echo I8N::_translate('settings') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('timezone') ?></label>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> <span class="caret"></span></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                </ul>
                            </div>
                        </div><!-- /btn-group -->
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('ntp_server') ?></label>
                    <div class="col-lg-4">
                        <label>
                            <input type="checkbox">
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('timeserver') ?></label>
                    <div class="col-lg-4">
                        <input type="url" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('date') ?></label>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <input type="date" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> <span class="glyphicon glyphicon-calendar"></span></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                </ul>
                            </div>
                        </div><!-- /btn-group -->
                    </div>
                </div>
                <div class="form-group form-inline">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('time') ?></label>
                    <div class="col-lg-1">
                        <div class="input-group">
                            <input type="date" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> <span class="caret"></span></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                </ul>
                                <label>:</label>
                            </div>
                        </div><!-- /btn-group -->
                    </div>
                    <div class="col-lg-1">
                        <div class="input-group">
                            <input type="date" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> <span class="caret"></span></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                </ul>
                                <label>:</label>
                            </div>
                        </div><!-- /btn-group -->
                    </div>

                    <div class="col-lg-1">
                        <div class="input-group">
                            <input type="date" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> <span class="caret"></span></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                </ul>
                                <label></label>
                            </div>
                        </div><!-- /btn-group -->
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>