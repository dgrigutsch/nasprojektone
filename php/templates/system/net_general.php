<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('general_settings') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">

            <fieldset>
                <legend><?php echo I8N::_translate('general_settings') ?></legend>
                <div class="form-group">
                    <div class="col-lg-6">
                        <label for="inputEmail1" class="col-lg-4 control-label"><?php echo I8N::_translate('hostname') ?></label>
                        <div class="input-group">
                            <input type="text" class="form-control">
                        </div><!-- /btn-group -->
                        <span class="help-block"><?php echo I8N::_translate('hostname_info') ?></span>
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-lg-6">
                        <label for="inputEmail1" class="col-lg-4 control-label"><?php echo I8N::_translate('domainname') ?></label>
                        <div class="input-group">
                            <input type="text" class="form-control">
                        </div><!-- /btn-group -->
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
