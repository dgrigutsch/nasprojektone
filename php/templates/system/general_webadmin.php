<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('general_settings') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">

            <fieldset>
                <legend><?php echo I8N::_translate('general_settings') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('port') ?></label>
                    <div class="col-lg-4">
                        <input type="number" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('session_limit') ?></label>
                    <div class="col-lg-4">
                        <input type="number" class="form-control" id="inputEmail1" placeholder="">
                        <span class="help-block"><?php echo I8N::_translate('session_limit_info') ?></span>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend><?php echo I8N::_translate('secure_connection') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('activate_ssl') ?></label>
                    <div class="col-lg-4">
                        <label>
                            <input type="checkbox"> <?php echo I8N::_translate('activate_ssl_info') ?>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('port') ?></label>
                    <div class="col-lg-4">
                        <input type="number" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('activate_ssl') ?></label>
                    <div class="col-lg-4">
                        <label>
                            <input type="checkbox"> <?php echo I8N::_translate('activate_ssl_info') ?>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('certificate') ?></label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> <span class="caret"></span></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                </ul>
                            </div>
                        </div><!-- /btn-group -->
                        <span class="help-block"><?php echo I8N::_translate('certificate_info') ?></span>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend><?php echo I8N::_translate('dns_discover_service') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('activate') ?></label>
                    <label>
                        <input type="checkbox"> <?php echo I8N::_translate('dns_discover_service_info') ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('name') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                        <span class="help-block"><?php echo I8N::_translate('service_name') ?></span>
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>


