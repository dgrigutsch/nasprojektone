<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('general_settings') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">

            <fieldset>
                <legend><?php echo I8N::_translate('settings') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('monitoring') ?></label>
                    <label>
                        <input type="checkbox"><?php echo I8N::_translate('monitoring_info') ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('powerswitch') ?></label>
                    <label>
                        <input type="checkbox"><?php echo I8N::_translate('powerswitch_info') ?>
                    </label>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>


