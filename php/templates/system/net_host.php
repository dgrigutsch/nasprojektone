<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('hosts') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">

            <fieldset>
                <legend><?php echo I8N::_translate('hosts') ?></legend>
                <div class="form-group">
                    <div class="col-lg-6">
                        <label for="inputEmail1" class="col-lg-4 control-label"><?php echo I8N::_translate('allow') ?></label>
                        <div class="input-group">
                            <textarea class="form-control" ></textarea>

                        </div><!-- /btn-group -->
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-lg-6">
                        <label for="inputEmail1" class="col-lg-4 control-label"><?php echo I8N::_translate('deny') ?></label>
                        <div class="input-group">
                            <textarea class="form-control" ></textarea>
                        </div><!-- /btn-group -->
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
