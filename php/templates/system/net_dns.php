<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('dns_service') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">

            <fieldset>
                <legend><?php echo I8N::_translate('dns_service') ?></legend>
                <div class="form-group">
                    <div class="col-lg-6">
                        <label for="inputEmail1" class="col-lg-4 control-label"><?php echo I8N::_translate('primary') ?></label>
                        <div class="input-group">
                            <input type="text" class="form-control">
                        </div><!-- /btn-group -->
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-lg-6">
                        <label for="inputEmail1" class="col-lg-4 control-label"><?php echo I8N::_translate('secondary') ?></label>
                        <div class="input-group">
                            <input type="text" class="form-control">
                        </div><!-- /btn-group -->
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>