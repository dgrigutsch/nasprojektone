<div class="panel panel-primary inner-panel">
    <div class="panel-heading"><?php echo I8N::_translate('email_notification') ?></div>
    <div class="panel-body">
        <form class="form-horizontal" role="form">

            <fieldset>
                <legend><?php echo I8N::_translate('email_notification') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('activate') ?></label>
                    <label>
                        <input type="checkbox">
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <legend><?php echo I8N::_translate('smtp_settings') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('smtp_server') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                        <span class="help-block"><?php echo I8N::_translate('smtp_server_info') ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('smtp_port') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                        <span class="help-block"><?php echo I8N::_translate('smtp_port_info') ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('use_ssl') ?></label>
                    <label>
                        <input type="checkbox">
                    </label>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('email_sender') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('email_auth') ?></label>
                    <label>
                        <input type="checkbox">
                    </label>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('username') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('password') ?></label>
                    <div class="col-lg-4">
                        <input type="password" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend><?php echo I8N::_translate('email_receiver') ?></legend>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('email_primary') ?></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label"><?php echo I8N::_translate('email_secondary') ?></label>
                    <div class="col-lg-4">
                        <input type="password" class="form-control" id="inputEmail1" placeholder="">
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
