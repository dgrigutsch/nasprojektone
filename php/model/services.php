<?php
/**
 * Klasse für den Datenzugriff
 */
class MODEL_SERVICES{

    //Einträge eines Blogs als zweidimensionales Array
    private static $entries = array(
        "apps"=>
        array(
            array("id"=>0, "title"=>"SNMP", "image"=>"images/apps/snmp.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>1, "title"=>"FTP", "image"=>"images/apps/ftp.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>2, "title"=>"TFTP", "image"=>"images/apps/tftp.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>3, "title"=>"SSH", "image"=>"images/apps/ssh.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>4, "title"=>"SMB/CIFS", "image"=>"images/apps/smb.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>5, "title"=>"RSYNC", "image"=>"images/apps/rsync.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>6, "title"=>"NFS", "image"=>"images/apps/nfs.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>7, "title"=>"Transmission", "image"=>"images/apps/transmission.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>8, "title"=>"GITLAB", "image"=>"images/apps/gitlab.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>9, "title"=>"owncloud", "image"=>"images/apps/owncloud.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>10, "title"=>"DLNA", "image"=>"images/apps/upnp.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>11, "title"=>"Plex", "image"=>"images/apps/plex.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>12, "title"=>"TVHEADEND", "image"=>"images/apps/tvheadend.png", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>13, "title"=>"MYTHTV", "image"=>"images/apps/mythtv.png", "content"=>"Ich bin der erste Eintrag."),
        )
    );

    /**
     * Gibt alle Einträge des Blogs zurück.
     *
     * @return Array Array von Blogeinträgen.
     */
    public static function getEntries(){
        return self::$entries;
    }

    /**
     * Gibt einen bestimmten Eintrag zurück.
     *
     * @param int $id Id des gesuchten Eintrags
     * @return Array Array, dass einen Eintrag repräsentiert, bzw.
     * 					wenn dieser nicht vorhanden ist, null.
     */
    public static function getEntry($id){
        if(array_key_exists($id, self::$entries)){
            return self::$entries[$id];
        }else{
            return "No Data";
        }
    }
}
?>