<?php
/**
 * Klasse für den Datenzugriff
 */
class MODEL_DIAGNOSTIC{

    //Einträge eines Blogs als zweidimensionales Array
    private static $entries = array(
        "processes"=>
        array(
            array("id"=>0, "title"=>"processes", "content"=>"Ich bin der erste Eintrag."),
        ),
        "logs"=>
        array(
            array("id"=>0, "title"=>"logs", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>1, "title"=>"settings", "content"=>"Ich bin der erste Eintrag.","view"=>"protocol_settings"),
        ),
        "systeminfo"=>
        array(
            array("id"=>0, "title"=>"overview", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>1, "title"=>"system", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>2, "title"=>"services", "content"=>"Ich bin der erste Eintrag."),
        )
    );

    /**
     * Gibt alle Einträge des Blogs zurück.
     *
     * @return Array Array von Blogeinträgen.
     */
    public static function getEntries(){
        return self::$entries;
    }

    /**
     * Gibt einen bestimmten Eintrag zurück.
     *
     * @param int $id Id des gesuchten Eintrags
     * @return Array Array, dass einen Eintrag repräsentiert, bzw.
     * 					wenn dieser nicht vorhanden ist, null.
     */
    public static function getEntry($id){
        if(array_key_exists($id, self::$entries)){
            return self::$entries[$id];
        }else{
            return "No Data";
        }
    }
}
?>