<?php
/**
 * Klasse für den Datenzugriff
 */
class MODEL_INFORMATION{

    //Einträge eines Blogs als zweidimensionales Array
    private static $entries = array(
        "donation"=>
        array(
            array("id"=>0, "title"=>"donation", "content"=>"Ich bin der erste Eintrag."),
        ),
        "support"=>
        array(
            array("id"=>0, "title"=>"support", "content"=>"Ich bin der erste Eintrag."),
        ),
        "about"=>
        array(
            array("id"=>0, "title"=>"about", "content"=>"Ich bin der erste Eintrag."),
        )
    );

    /**
     * Gibt alle Einträge des Blogs zurück.
     *
     * @return Array Array von Blogeinträgen.
     */
    public static function getEntries(){
        return self::$entries;
    }

    /**
     * Gibt einen bestimmten Eintrag zurück.
     *
     * @param int $id Id des gesuchten Eintrags
     * @return Array Array, dass einen Eintrag repräsentiert, bzw.
     * 					wenn dieser nicht vorhanden ist, null.
     */
    public static function getEntry($id){
        if(array_key_exists($id, self::$entries)){
            return self::$entries[$id];
        }else{
            return "No Data";
        }
    }
}
?>