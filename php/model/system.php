<?php
/**
 * Klasse für den Datenzugriff
 */

class MODEL_SYSTEM{

    //Einträge eines Blogs als zweidimensionales Array
    private static $entries = array(
        "general_settings"=>
        array(
            array("id"=>0, "title"=>"web_admin", "content"=>"kllhlIch bin der erste Eintrag.", "view"=>"general_webadmin" ),
            array("id"=>1, "title"=>"web_pass", "content"=>"gkgIch bin der erste Eintrag", "view"=>"general_webpass"),
        ),
        "time_date"=>
        array(
            array("id"=>0, "title"=>"time_date", "content"=>"Ich bin der erste Eintrag.", "view"=>"time_time"),
        ),
        "network"=>
        array(
            array("id"=>0, "title"=>"general", "content"=>"Ich bin der erste Eintrag.","view"=>"net_general"),
            array("id"=>1, "title"=>"interfaces", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>2, "title"=>"dns_service", "content"=>"Ich bin der erste Eintrag.","view"=>"net_dns"),
            array("id"=>3, "title"=>"hosts", "content"=>"Ich bin der erste Eintrag.","view"=>"net_host"),
            array("id"=>4, "title"=>"firewall", "content"=>"Ich bin der erste Eintrag."),
        ),
        "email_notification"=>
        array(
            array("id"=>0, "title"=>"email_notification", "content"=>"Ich bin der erste Eintrag.","view"=>"email_general"),
        ),
        "energiemanagement"=>
        array(
            array("id"=>0, "title"=>"settings", "content"=>"Ich bin der erste Eintrag.","view"=>"energie_general"),
            array("id"=>1, "title"=>"timeplan", "content"=>"Ich bin der erste Eintrag."),
        ),
        "certificates"=>
        array(
            array("id"=>0, "title"=>"certificates", "content"=>"Ich bin der erste Eintrag."),
        ),
        "cronjobs"=>
        array(
            array("id"=>0, "title"=>"cronjobs", "content"=>"Ich bin der erste Eintrag."),
        ),
        "updatesmanagement"=>
        array(
            array("id"=>0, "title"=>"updatesmanagement", "content"=>"Ich bin der erste Eintrag."),
        ),
        "addons"=>
        array(
            array("id"=>0, "title"=>"addons", "content"=>"", "href"=>"index.php?view=services&id=apps"),
        )
    );

    /**
     * Gibt alle Einträge des Blogs zurück.
     *
     * @return Array Array von Blogeinträgen.
     */
    public static function getEntries(){
        return self::$entries;
    }

    /**
     * Gibt einen bestimmten Eintrag zurück.
     *
     * @param int $id Id des gesuchten Eintrags
     * @return Array Array, dass einen Eintrag repräsentiert, bzw.
     * 					wenn dieser nicht vorhanden ist, null.
     */
    public static function getEntry($id){
        if(array_key_exists($id, self::$entries)){
            return self::$entries[$id];
        }else{
            return "No Data";
        }
    }
}
?>