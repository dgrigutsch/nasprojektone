<?php
/**
 * Klasse für den Datenzugriff
 */
class MODEL_USERS{

    //Einträge eines Blogs als zweidimensionales Array
    private static $entries = array(
        "user"=>
        array(
            array("id"=>0, "title"=>"user", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>1, "title"=>"settings", "content"=>"Ich bin der erste Eintrag."),
        ),
        "group"=>
        array(
            array("id"=>0, "title"=>"group", "content"=>"Ich bin der erste Eintrag."),
        ),
        "public_folder"=>
        array(
            array("id"=>0, "title"=>"public_folder", "content"=>"Ich bin der erste Eintrag."),
        )
    );

    /**
     * Gibt alle Einträge des Blogs zurück.
     *
     * @return Array Array von Blogeinträgen.
     */
    public static function getEntries(){
        return self::$entries;
    }

    /**
     * Gibt einen bestimmten Eintrag zurück.
     *
     * @param int $id Id des gesuchten Eintrags
     * @return Array Array, dass einen Eintrag repräsentiert, bzw.
     * 					wenn dieser nicht vorhanden ist, null.
     */
    public static function getEntry($id){
        if(array_key_exists($id, self::$entries)){
            return self::$entries[$id];
        }else{
            return "No Data";
        }
    }
}
?>