<?php
/**
 * Klasse für den Datenzugriff
 */
class MODEL_STORAGE{

    //Einträge eines Blogs als zweidimensionales Array
    private static $entries = array(
        "disks"=>
        array(
            array("id"=>0, "title"=>"realdisks", "content"=>"Ich bin der erste Eintrag."),
        ),
        "raid"=>
        array(
            array("id"=>0, "title"=>"raid", "content"=>"Ich bin der erste Eintrag."),
        ),
        "datasystem"=>
        array(
            array("id"=>0, "title"=>"datasystem", "content"=>"Ich bin der erste Eintrag."),
        ),
        "smart"=>
        array(
            array("id"=>0, "title"=>"settings", "content"=>"Ich bin der erste Eintrag.","view"=>"smart_general"),
            array("id"=>1, "title"=>"disks", "content"=>"Ich bin der erste Eintrag."),
            array("id"=>2, "title"=>"timed_tests", "content"=>"Ich bin der erste Eintrag."),
        )
    );

    /**
     * Gibt alle Einträge des Blogs zurück.
     *
     * @return Array Array von Blogeinträgen.
     */
    public static function getEntries(){
        return self::$entries;
    }

    /**
     * Gibt einen bestimmten Eintrag zurück.
     *
     * @param int $id Id des gesuchten Eintrags
     * @return Array Array, dass einen Eintrag repräsentiert, bzw.
     * 					wenn dieser nicht vorhanden ist, null.
     */
    public static function getEntry($id){
        if(array_key_exists($id, self::$entries)){
            return self::$entries[$id];
        }else{
            return "No Data";
        }
    }
}
?>