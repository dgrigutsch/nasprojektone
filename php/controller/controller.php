<?php

//include('php/model/system.php');

//function __autoload($class_name) {
//    require_once('php/model/'.$class_name.'.php');
//}
function include_all_php($folder){
    foreach (glob("{$folder}/*.php") as $filename)
    {
        include $filename;
    }
}
include('php/i8n/translate.php');
include_all_php("php/model");
include_all_php("php/view");


class Routes {

    public static $curLanguage = "german";

    public static function getRoutes(){
        return array(
            'system' =>array('view'=>'system','model'=>MODEL_SYSTEM::getEntries(),'tpl'=>'system','title'=>I8N::translate(self::$curLanguage,'system')),
            'storage' =>array('view'=>'storage','model'=>MODEL_STORAGE::getEntries(),'tpl'=>'storage','title'=>I8N::translate(self::$curLanguage,'storage')),
            'users' =>array('view'=>'users','model'=>MODEL_USERS::getEntries(),'tpl'=>'system','title'=>I8N::translate(self::$curLanguage,'users')),
            'services' =>array('view'=>'services','model'=>MODEL_SERVICES::getEntries(),'tpl'=>'apps','title'=>I8N::translate(self::$curLanguage,'services')),
            'diagnostics' =>array('view'=>'diagnostics','model'=>MODEL_DIAGNOSTIC::getEntries(),'tpl'=>'diagnostics','title'=>I8N::translate(self::$curLanguage,'diagnostics')),
            'informations' =>array('view'=>'informations','model'=>MODEL_INFORMATION::getEntries(),'tpl'=>'system','title'=>I8N::translate(self::$curLanguage,'informations'))
        );
    }
    public static function getRoute($tpl){
        if(array_key_exists($tpl,Routes::getRoutes())){
            return Routes::getRoutes()[$tpl];
        }
    }
}

class Controller{

	private $request = null;
	private $template = '';
	private $view = null;

	/**
	 * Konstruktor, erstellet den Controller.
	 *
	 * @param Array $request Array aus $_GET & $_POST.
	 */
	public function __construct($request){

		$this->view = new View();
		$this->request = $request;
		$this->template = !empty($request['view']) ? $request['view'] : 'default';
	}

	/**
	 * Methode zum anzeigen des Contents.
	 *
	 * @return String Content der Applikation.
	 */
	public function display(){
		$view = new View();

        if(array_key_exists($this->template,Routes::getRoutes())){
            $array = Routes::getRoute($this->template);
            $view->setTemplate($array['tpl']);
            $entryid = $this->request['id'];
            $entry = $array['model'][$entryid];
            $view->assign('entries', $entry);
        } else {
            $entries = Model::getEntries();
            $view->setTemplate('default');
            $view->assign('entries', $entries);
        }

		$this->view->setTemplate('theblog');
		//$this->view->assign('blog_title', 'Der Titel des Blogs');
		//$this->view->assign('blog_footer', 'Ein Blog von und mit MVC');
		$this->view->assign('blog_content', $view->loadTemplate());
		return $this->view->loadTemplate();
	}
}
?>