<?php

class I8N {
    public static function translate($language,$word){
        $file = 'php/i8n/'.$language.'.lang'.'.php';
        if(file_exists($file)){
            include_once('php/i8n/'.$language.'.lang'.'.php');
        }else {
            include_once('php/i8n/german.lang.php');
        }
        return LANGUAGE::getLanguage($word);
    }

    public static function _translate($word){
        $file = 'php/i8n/'.Routes::$curLanguage.'.lang'.'.php';
        if(file_exists($file)){
            include_once('php/i8n/'.Routes::$curLanguage.'.lang'.'.php');
        }else {
            include_once('php/i8n/german.lang.php');
        }
        return LANGUAGE::getLanguage($word);
    }
}