var LeftMenuOptions = {
    systen:{
        title: "System",
        suboptions : [
            {title:"Allgemeine Einstellungen",link:"layouts/general_settings.html"},
            {title:"Datum & Zeit",link:"general_settings"},
            {title:"Email Benachrichtigungen",link:"general_settings"},
            {title:"Energieverwaltung",link:"general_settings"},
            {title:"Zertifikate",link:"general_settings"},
            {title:"Cron Jobs",link:"general_settings"},
            {title:"Aktualisierungsverwaltung",link:"general_settings"},
            {title:"Erweiterungen",link:"general_settings"},
        ]
    },
    storage:{
        title : "Datenspeicher",
        suboptions : [
            {title:"Reale Festplatten",link:"general_settings"},
            {title:"RAID Verwaltung",link:"general_settings"},
            {title:"Dateisysteme",link:"general_settings"},
            {title:"S.M.A.R.T.",link:"general_settings"},
        ]
    },
    users:{
        title : "Zugriffskontrolle",
        suboptions : [
            {title:"Benutzer",link:"general_settings"},
            {title:"Gruppe",link:"general_settings"},
            {title:"Freigegebene Ordner",link:"general_settings"},
        ]
    },
    services:{
        title : "Dienste",
        suboptions : [
            {title:"Bittorrent",link:"general_settings"},
            {title:"SNMP",link:"general_settings"},
            {title:"FTP",link:"general_settings"},
            {title:"TFTP",link:"general_settings"},
            {title:"SSH",link:"general_settings"},
            {title:"SMB/CIFS",link:"general_settings"},
            {title:"RSYNC",link:"general_settings"},
            {title:"NFS",link:"general_settings"},
        ]
    },
    diagnostics:{
        title : "Diagnose",
        suboptions : [
            {title:"Prozesse",link:"general_settings"},
            {title:"Systemprotokolle",link:"general_settings"},
            {title:"Systeminformationen",link:"general_settings"},
        ]
    },
    informations:{
        title : "Informationen",
        suboptions : [
            {title:"Spenden",link:"general_settings"},
            {title:"Support",link:"general_settings"},
            {title:"Über",link:"general_settings"},
        ]
    }
}

